package ticker

import (
	"fmt"
	"testing"
	"time"
)

func TestNew(t *testing.T) {
	ticker := New(time.Millisecond)
	for i := 0; i < 10; i++ {
		fmt.Println(<-ticker.C)
	}
}

func BenchmarkNew(t *testing.B) {
	for i := 0; i < t.N; i++ {
		New(time.Microsecond)
	}
}
