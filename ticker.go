// Package ticker contains a minimalist reimplementation of time.Ticker.
package ticker

import "time"

type Ticker struct {
	C     <-chan time.Time
	stopC chan interface{}
}

func New(d time.Duration) *Ticker {
	current := time.Now()
	c := make(chan time.Time, 1)
	t := Ticker{C: c, stopC: make(chan interface{})}
	go func() {
		for {
			select {
			case <-t.stopC:
				return
			default:
				next := current.Add(d)
				sleepDuration := next.Sub(time.Now())
				time.Sleep(sleepDuration)
				c <- next
				current = next
			}
		}
	}()
	return &t
}

func (t *Ticker) Stop() { t.stopC <- nil }
